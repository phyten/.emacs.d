(deftheme phyten
  "Created 2019-12-09.")

(custom-theme-set-variables
 'phyten
 '(ansi-color-faces-vector [default default default italic underline success warning error])
 '(session-use-package t)
 '(custom-safe-themes (quote ("c382589a7610a4984a091577c8289634de06afbea03ff6016ebabab5d2a5d60b" default)))
 '(display-time-mode t)
 '(tool-bar-mode nil)
 '(transient-mark-mode t))

(custom-theme-set-faces
 'phyten
 '(hiwin-face ((t (:box nil))))
 '(web-mode-comment-face ((t (:foreground "#587F35"))))
 '(web-mode-css-at-rule-face ((t (:foreground "#DFCF44"))))
 '(web-mode-css-property-name-face ((t (:foreground "#87CEEB"))))
 '(web-mode-css-selector-face ((t (:foreground "#DFCF44"))))
 '(web-mode-css-string-face ((t (:foreground "#D78181"))))
 '(web-mode-doctype-face ((t (:foreground "#4A8ACA"))))
 '(web-mode-html-attr-equal-face ((t (:foreground "#FFFFFF"))))
 '(web-mode-html-attr-name-face ((t (:foreground "#87CEEB"))))
 '(web-mode-html-attr-value-face ((t (:foreground "#D78181"))))
 '(web-mode-html-tag-bracket-face ((t (:foreground "#4A8ACA"))))
 '(web-mode-html-tag-face ((t (:foreground "#4A8ACA"))))
 '(cursor ((t (:background "black")))))

(provide-theme 'phyten)
