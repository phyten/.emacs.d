;; -*- lexical-binding: t -*-
(setq debug-on-error nil)

;;; This file bootstraps the configuration, which is divided into
;;; a number of other files.

(let ((minver "24.3"))
  (when (version< emacs-version minver)
    (error "Your Emacs is too old -- this config requires v%s or higher" minver)))
(when (version< emacs-version "24.5")
  (message "Your Emacs is old, and some functionality in this config will be disabled. Please upgrade if possible."))

(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
(add-to-list 'load-path "/Users/phyten/.emacs.d/site-lisp")
(require 'init-benchmarking) ;; Measure startup time

(defconst *spell-check-support-enabled* nil) ;; Enable with t if you prefer
(defconst *is-a-mac* (eq system-type 'darwin))

;;----------------------------------------------------------------------------
;; Adjust garbage collection thresholds during startup, and thereafter
;;----------------------------------------------------------------------------
(let ((normal-gc-cons-threshold (* 20 1024 1024))
      (init-gc-cons-threshold (* 128 1024 1024)))
  (setq gc-cons-threshold init-gc-cons-threshold)
  (add-hook 'after-init-hook
            (lambda () (setq gc-cons-threshold normal-gc-cons-threshold))))

;;----------------------------------------------------------------------------
;; Bootstrap config
;;----------------------------------------------------------------------------
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(require 'init-utils)
(require 'init-site-lisp) ;; Must come before elpa, as it may provide package.el
;; Calls (package-initialize)
(require 'init-elpa)      ;; Machinery for installing required packages
(require 'init-exec-path) ;; Set up $PATH

;;----------------------------------------------------------------------------
;; Allow users to provide an optional "init-preload-local.el"
;;----------------------------------------------------------------------------
(require 'init-preload-local nil t)

;;----------------------------------------------------------------------------
;; Load configs for specific features and modes
;;----------------------------------------------------------------------------

(require-package 'bind-key)
(require-package 'wgrep)
(require-package 'diminish)
(require-package 'scratch)
(require-package 'command-log-mode)

(require 'init-recentf)
(require 'init-smex)
(require 'init-ivy)
;;(require 'init-helm)
(require 'init-hippie-expand)
(require 'init-company)
(require 'init-windows)
(require 'init-sessions)
(require 'init-fonts)
(require 'init-mmm)

(require 'init-editing-utils)
(require 'init-whitespace)

(require 'init-vc)
(require 'init-darcs)
(require 'init-git)
(require 'init-github)

(require 'init-projectile)

(require 'init-compile)
;;(require 'init-crontab)
(require 'init-textile)
(require 'init-markdown)
(require 'init-csv)
(require 'init-erlang)
(require 'init-javascript)
(require 'init-php)
(require 'init-org)
(require 'init-nxml)
(require 'init-html)
(require 'init-css)
(require 'init-haml)
(require 'init-http)
(require 'init-python)
(require 'init-haskell)
(require 'init-elm)
(require 'init-purescript)
(require 'init-ruby)
(require 'init-rails)
(require 'init-sql)
(require 'init-rust)
(require 'init-toml)
(require 'init-yaml)
(require 'init-docker)
(require 'init-terraform)
;;(require 'init-nix)
(maybe-require-package 'nginx-mode)

(require 'init-paredit)
(require 'init-lisp)
(require 'init-slime)
(require 'init-clojure)
(require 'init-clojure-cider)
(require 'init-common-lisp)

(when *spell-check-support-enabled*
  (require 'init-spelling))

(require 'init-misc)

(require 'init-folding)
(require 'init-dash)

;;(require 'init-twitter)
;; (require 'init-mu)
(require 'init-ledger)
;; Extra packages which don't require any configuration

(require-package 'gnuplot)
(require-package 'lua-mode)
(require-package 'htmlize)
(require-package 'dsvn)
(when *is-a-mac*
  (require-package 'osx-location))
(unless (eq system-type 'windows-nt)
  (maybe-require-package 'daemons))
(maybe-require-package 'dotenv-mode)

(when (maybe-require-package 'uptimes)
  (setq-default uptimes-keep-count 200)
  (add-hook 'after-init-hook (lambda () (require 'uptimes))))


;;----------------------------------------------------------------------------
;; Allow access from emacsclient
;;----------------------------------------------------------------------------
(require 'server)
(unless (server-running-p)
  (server-start))

;;----------------------------------------------------------------------------
;; Variables configured via the interactive 'customize' interface
;;----------------------------------------------------------------------------
(when (file-exists-p custom-file)
  (load custom-file))


;;----------------------------------------------------------------------------
;; Locales (setting them earlier in this file doesn't work in X)
;;----------------------------------------------------------------------------
(require 'init-locales)


;;----------------------------------------------------------------------------
;; Allow users to provide an optional "init-local" containing personal settings
;;----------------------------------------------------------------------------
(require 'init-local nil t)



(provide 'init)

(require-package 'bind-key)

;; =======================================================================
;; 基本的なキーバインドの変更及び追加
;; =======================================================================
;; (load-theme 'manoj-dark t) (load-theme `monokai t)

(add-to-list 'default-frame-alist '(font . "ricty-18"))
;; (add-to-list 'default-frame-alist '(font . "Droid Sans Mono"))

;; UNDO
(require 'undo-tree)
(bind-key "C-z" 'undo)
(global-undo-tree-mode t)
(bind-key "M-z" 'undo-tree-redo)
;; Yank
(bind-key "C-y" 'yank)
;; Ctrl-Hでバックスペース
(bind-key* "C-h" 'backward-delete-char)
;;; company-modeでもC-hでバックスペース
(with-eval-after-load 'company
  (bind-key "C-h" nil company-active-map)
  ;; (define-key company-active-map (kbd "C-h") nil)
  )
;; Meta-Hで単語分バックスペース
(bind-key* "M-h" 'backward-kill-word)
;; M-aでインデントが終わった後まで戻る
(bind-key* "M-a" 'back-to-indentation)

;; Xのクリップボードとemacsのkillringを共有
(cond (window-system
       (setq x-select-enable-clipboard t)
       ))

;; ¥の代わりにバックスラッシュを入力する
(define-key global-map [?¥] [?\\])

;; Modifierの設定
(setq mac-command-modifier 'super)
(setq mac-option-modifier 'meta)
(setq mac-right-option-modifier 'hyper)
(setq mac-right-command-modifier 'alt)

;;; タブの代わりに半角スペースを使う
(setq-default tab-width 4 indent-tabs-mode nil)

;;; C-x C-xでリージョン選択
(bind-key* "C-x C-x" 'set-mark-command )

 ;;; C-hがisearch内でもバックスペースになるように変更
(bind-key "C-h" 'isearch-delete-char isearch-mode-map)

;; interlocking fileを作成しない、ただしemacs24.3以降限定
(setq create-lockfiles nil)

;;; 循環コマンド
(require-package 'sequential-command)
(require 'sequential-command-config)
(define-sequential-command seq-home
  beginning-of-line back-to-indentation beginning-of-buffer seq-return)
(bind-key "C-a" 'seq-home)
(bind-key "C-e" 'seq-end)
(when (require 'org nil t)
  (bind-key "C-a" 'org-seq-home org-mode-map)
  (bind-key "C-e" 'org-seq-end org-mode-map))
(bind-key "u" 'seq-upcase-backward-word esc-map)
(bind-key "c" 'seq-capitalize-backward-word esc-map)
(bind-key "l" 'seq-downcase-backward-word esc-map)

;;; elscreenコマンド
(require-package 'elscreen)
(setq elscreen-prefix-key (kbd "C-q"))
(bind-key* "C-M-h" 'elscreen-previous)
(bind-key* "C-M-l" 'elscreen-next)
(bind-key* "C-q C-c" 'elscreen-clone);; (setq elscreen-display-tab nil)

 ;;;  Use frame-title for tabs
;; How to display the list of screens on the frame-title of my Emacs?
;; This is broken. get-alist should be changed to alist-get
;; https://www.emacswiki.org/emacs/EmacsLispScreen#toc8
;;
(defvar *elscreen-tab-truncate-length*
  20 "Number of characters to truncate tab names in frame title")
;;
(defun elscreen-tabs-as-string ()
  "Return a string representation of elscreen tab names

 Set name truncation length in ELSCREEN-TRUNCATE-LENGTH"
  (let* ((screen-list (sort (elscreen-get-screen-list) '<))
         (screen-to-name-alist (elscreen-get-screen-to-name-alist)))
    ;; mapconcat: mapping and then concate name elements together with separator
    (mapconcat
     (lambda (screen)
       (format (if (string-equal "+" (elscreen-status-label screen))
                   ;; Current screen format
                   "[ %d ] %s"
                 ;; Others
                 "(%d) %s")
               ;; screen number: replaces %d (integer)
               screen
               ;; screen name: replaces %s (string)
               (elscreen-truncate-screen-name
                ;; Return the value associated with KEY in ALIST
                (alist-get screen screen-to-name-alist)
                *elscreen-tab-truncate-length*)))
     ;; Screen numbers (keys for alist)
     screen-list
     ;; Separator
     " | ")))
;;
(defvar *elscreen-tabs-as-string*
  "" "Variable to hold curent elscreen tab names as a string")
;;
(defun update-elscreen-tabs-as-string ()
  "Update *elscreen-tabs-as-string* variable"
  (interactive)
  (setq *elscreen-tabs-as-string* (elscreen-tabs-as-string)))
;;
;; Update *elscreen-tabs-as-string* whenever elscreen status updates
(add-hook 'elscreen-screen-update-hook 'update-elscreen-tabs-as-string)
;;
;; Set frame title format as combination of current elscreen tabs and buffer/path
;; (setq frame-title-format '(:eval (concat *elscreen-tabs-as-string*
;;                                          "    ||    "
;;                                          (if buffer-file-name
;;                                              (abbreviate-file-name buffer-file-name)
;;                                            "%b"))))
(setq frame-title-format "EmacsMac")

(elscreen-start)

(add-hook 'elisp-mode-hook 'rainbow-mode)

(defun insert_semicolon_for_minus ()
  (interactive)
  (end-of-line)
  (insert ";")
  )

(bind-key "C--" 'insert_semicolon_for_minus)

(bind-key* "C-M--" 'comment-dwim)

(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
(add-hook 'web-mode-hook 'emmet-mode)
;; (add-to-list 'auto-mode-alist '("\\.php\\'" . emmet-mode))

;; .js, .jsx を web-mode で開く
;; (add-to-list 'auto-mode-alist '("\\.js[x]?$" . web-mode))

;; 拡張子 .js でもJSX編集モードに
;; (setq web-mode-content-types-alist
;;       '(("jsx" . "\\.js[x]?\\'")))

;; インデント
(add-hook 'web-mode-hook
          '(lambda ()
             (setq web-mode-attr-indent-offset nil)
             (setq web-mode-markup-indent-offset 2)
             (setq web-mode-css-indent-offset 2)
             (setq web-mode-code-indent-offset 2)
             (setq web-mode-sql-indent-offset 2)
             (setq indent-tabs-mode nil)
             (setq tab-width 2)
             ))

(require 'ruby-electric)
(add-hook 'ruby-mode-hook '(lambda () (ruby-electric-mode t)))
;; (setq ruby-electric-expand-delimiters-list nil)

;; 自分用・追加用テンプレート -> mysnippetに作成したテンプレートが格納される
(require 'yasnippet)
(setq yas-snippet-dirs
      '("~/.emacs.d/mysnippets"
        "~/.emacs.d/yasnippets"
        ))

;; 既存スニペットを挿入する
;; (define-key yas-minor-mode-map (kbd "C-x C-i") 'yas-insert-snippet)
;; 新規スニペットを作成するバッファを用意する
(bind-key (kbd "C-x i n") 'yas-new-snippet yas-minor-mode-map)
;; 既存スニペットを閲覧・編集する
(bind-key (kbd "C-x i v") 'yas-visit-snippet-file yas-minor-mode-map)

(yas-global-mode 1)

(yas-reload-all)
(add-hook 'web-mode-hook #'yas-minor-mode)

(defun couns-git ()
  "Find file in the current Git repository."
  (interactive)
  (let* ((default-directory (locate-dominating-file
                             default-directory ".git"))
         (cands (split-string
                 (shell-command-to-string
                  "git ls-files --full-name --")
                 "\n"))
         (file (ivy-read "Find file: " cands)))
    (when file
      (find-file file))))

(require 'key-chord)
(key-chord-mode 1)

(key-chord-define-global "jk" 'find-file-in-project)
(key-chord-define-global "df" 'counsel-recentf)
;; (key-chord-define-global "hj" 'yas-insert-snippet)
(global-set-key (kbd "C-x C-i") 'company-yasnippet)


;; (global-set-key (kbd "C-c f") 'couns-git)
;; (put 'set-goal-column 'disabled nil)

;; interlocking fileを作成しない、ただしemacs24.3以降限定
(setq create-lockfiles nil)

;; (setq linum-delay t)
;; (defadvice linum-schedule (around my-linum-schedule () activate)
;;   (run-with-idle-timer 0.2 nil #'linum-update-current))

;;; wdired
(require 'wdired)
(setq wdired-allow-to-change-permissions t)
(define-key dired-mode-map "e" 'wdired-change-to-wdired-mode)

;;; 全てのbufferをkillする
(defun close-all-buffers ()
  (interactive)
  (mapc 'kill-buffer (buffer-list)))

(global-set-key "\C-s" 'swiper)
(defvar swiper-include-line-number-in-search t)

;;; dumb-jump
(setq dumb-jump-mode t)
(setq dumb-jump-selector 'ivy) ;; 候補選択をivyに任せます
(setq dumb-jump-use-visible-window nil)
(define-key global-map [(super d)] 'dumb-jump-go) ;; go-to-definition!
(define-key global-map [(super shift d)] 'dumb-jump-back)

;;; 便利なbuffer切り替え
(bind-key* (kbd "<backtab>") 'iflipb-next-buffer)
(bind-key* (kbd "C-M-i") 'iflipb-next-buffer)
(bind-key* [(meta shift tab)] 'iflipb-previous-buffer)
(with-eval-after-load 'magit
  (bind-key "<M-tab>" nil magit-process-mode-map)
  (bind-key [(meta tab)] nil magit-process-mode-map)
  (bind-key (kbd "<backtab>") nil magit-process-mode-map)
  (bind-key "<M-tab>" nil magit-status-mode-map)
  (bind-key [(meta tab)] nil magit-status-mode-map)
  (bind-key (kbd "<backtab>") nil magit-status-mode-map)
  (bind-key "<M-tab>" nil magit-diff-mode-map)
  (bind-key [(meta tab)] nil magit-diff-mode-map)
  (bind-key (kbd "<backtab>") nil magit-diff-mode-map)
  )

;;; multiple-cursors
(require 'multiple-cursors)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)

;;; open-junk-file
(when (require 'open-junk-file)
  (setq open-junk-file-format "~/Documents/junk/%Y-%m-%d-%H%M%S.")
  (global-set-key (kbd "C-x j") 'open-junk-file))

;;; counsel-yank-pop
(global-set-key (kbd "M-y") 'counsel-yank-pop)

;;; web-mode highlight
(setq web-mode-enable-current-element-highlight t)
(setq web-mode-enable-current-column-highlight t)

;;; hide-mode-line
(require 'hide-mode-line)
(add-hook 'neotree-mode #'hide-mode-line-mode)

;;; smooth-scroll
;; 縦方向のスクロール行数を変更する。
;; (setq smooth-scroll/vscroll-step-size 4)
;; 横方向のスクロール行数を変更する。
;; (setq smooth-scroll/hscroll-step-size 4)

(require 'warnings)
(add-to-list 'warning-suppress-types '(yasnippet backquote-change))

;;; ブラウザとかで開いてくれるやつ
(global-set-key (kbd "C-<f5>") 'browse-url-of-file)

;;; open current directory
(defun finder-current-dir-open()
  (interactive)
  (shell-command "open .")) ;; 補足あり

(bind-key* "C-x C-M-f" 'finder-current-dir-open)

;;; open selected directory
(defun finder-open(dirname)
  (interactive "DDirectoryName:")
  (shell-command (concat "open " dirname)))

(require 'hydra)
(bind-key
 [f1]
 (defhydra hydra-zoom (:color red :hint nil)
   "zoom"
   ("b" default-text-scale-increase "big")
   ("s" default-text-scale-decrease "small")
   ("r" default-text-scale-reset "reset")))

;;; hydra-rails
(bind-key
 [f2]
 (defhydra hydra-rails (:color pink :hint nil)
   "
    Relation		Directory
    ---------------	---------------
    _m_: Model		_M_: app/models
    _c_: Controller	_C_: app/controllers
    _v_: View			_V_: app/views
    _i_: Migration	_H_: app/helpers
                    _L_: lib
                    _I_: db/migrate
                    _J_: app/assets/javascripts
                    _S_: app/assets/stylesheets
                    _A_: app/mailers
                    _T_: lib/tasks

   "
   ("m" projectile-rails-find-current-model)
   ("c" projectile-rails-find-current-controller)
   ("v" projectile-rails-find-current-view)
   ("i" projectile-rails-find-current-migration)
   ("M" projectile-rails-find-model)
   ("C" projectile-rails-find-controller)
   ("V" projectile-rails-find-view)
   ("H" projectile-rails-find-helper)
   ("L" projectile-rails-find-lib)
   ("I" projectile-rails-find-migration)
   ("J" projectile-rails-find-javascript)
   ("S" projectile-rails-find-stylesheet)
   ("A" projectile-rails-find-mailer)
   ("T" projectile-rails-find-rake-task)
   ("q" nil "quit")))

;;; web-modeのweb-mode-engineによって、読み込むyasnippetを変える
(require 'web-mode)
(defcustom my-web-yas-mode-alist
  '(("erb" . erb)
    ("php" . php))
  ""
  :group 'web-mode
  :type '(repeat (cons
                  :tag "Map engine name to mode"
                  (string :tag "Engine Name")
                  (symbol :tag "Mode"))))

(defun my-web-setup-yas ()
  "Setup yas according to `web-mode-engine`."
  (require 'yasnippet)
  (let ((extra-mode (cdr (assoc-string web-mode-engine my-web-yas-mode-alist))))
    (when extra-mode
      (yas-activate-extra-mode extra-mode))))

(advice-add 'web-mode-on-engine-setted :after #'my-web-setup-yas)

;;; windowをpx単位で変えられるように
(setq frame-resize-pixelwise t)


;;; tern-mode(nodeのプラグインを用いたjsの補完)
;; depth絡みの不具合への対策
;; override
(defun company-tern-depth (candidate)
  "Return depth attribute for CANDIDATE. 'nil' entries are treated as 0."
  (let ((depth (get-text-property 0 'depth candidate)))
    (if (eq depth nil) 0 depth)))

(defun js2-company-tern-hook ()
  (when (locate-library "tern")
    ;; .tern-port を作らない
    (setq tern-command '("tern" "--no-port-file"))
    (tern-mode t)))

;; (add-hook 'js2-mode-hook 'js2-company-tern-hook)

;; backend追加
;; (add-to-list 'company-backends 'company-tern)
;; company-dabbrev-codeは現在開いているバッファからワードを拾ってくる
;; IdとIDとかの表記ゆれにやられるのであえて切っている
;; (add-to-list 'company-backends '(company-tern :with company-dabbrev-code))

;; css-mode scss-modeでコロンを入力したら自動的に末尾にセミコロンを追加
(defun semicolon-ret ()
  (interactive)
  (insert ":")
  (insert " ")
  (insert ";")
  (backward-char)
  )
(defun brace-ret-brace ()
  (interactive)
  (insert "{") (newline-and-indent)
  (newline-and-indent)
  (insert "}") (indent-for-tab-command)
  (previous-line)
  (indent-for-tab-command)
  )
(defun space-check ()
  (interactive)
  (backward-char )
  (if (looking-at ":")
      (progn
        (forward-char )
        (insert " ;")
        (backward-char )
        )
    (progn
      (forward-char )
      (insert " "))))

(add-hook 'css-mode-hook
          (lambda ()
            (setq css-indent-offset 2)
            (bind-key "<SPC>" 'space-check css-mode-map)
            (bind-key "{" 'brace-ret-brace css-mode-map)
            ))

(add-hook 'scss-mode-hook
          (lambda ()
            (setq css-indent-offset 2)
            (bind-key "<SPC>" 'space-check scss-mode-map)
            (bind-key "{" 'brace-ret-brace scss-mode-map)
            ))

(setq ivy-do-completion-in-region nil)
(add-hook 'ivy-mode-hook
          (lambda ()
            (setq ivy-initial-inputs-alist nil)
            ))
(bind-key* (kbd "C-M-t") 'neotree-toggle)

(setq backup-directory-alist '((".*" . "~/.ehist")))

;; 番号付けによる複数保存
(setq version-control     t)  ;; 実行の有無
(setq kept-new-versions   5)  ;; 最新の保持数
(setq kept-old-versions   1)  ;; 最古の保持数
(setq delete-old-versions t)  ;; 範囲外を削除

;; alpha
(if window-system
    (progn
      (set-frame-parameter nil 'alpha 100)))

;; 非アクティブウィンドウの背景色を設定
;; (require 'hiwin)
;; (hiwin-activate)
;; (set-face-background 'hiwin-face "gray30")

;; line numberの表示
;; (require 'linum)
;; (global-linum-mode 1)

;; tabサイズ
(setq default-tab-width 4)

;; default scroll bar消去
(scroll-bar-mode -1)


;; 現在ポイントがある関数名をモードラインに表示
(which-function-mode 1)

;; 対応する括弧をハイライト
(show-paren-mode 1)

;; リージョンのハイライト
(transient-mark-mode 1)

;;; mode-lineを設定
(defvar sml/no-confirm-load-theme t)
(defvar sml/theme 'light) ;; お好みで
(defvar sml/shorten-directory -1) ;; directory pathはフルで表示されたいので
(sml/setup)
(require 'diminish)
(eval-after-load "company" '(diminish 'company-mode "Comp"))
(eval-after-load "ivy" '(diminish 'ivy-mode))
(column-number-mode t) ;; 列番号の表示
(line-number-mode t) ;; 行番号の表示

;; 総行数
(require 'total-lines)
(global-total-lines-mode t)
(defun my-set-line-numbers ()
  (setq-default mode-line-front-space
                (append mode-line-front-space
                        '((:eval (format " (%d)" (- total-lines 1))))))) ;; 「" (%d)"」の部分はお好みで
(add-hook 'after-init-hook 'my-set-line-numbers)

;; mode line を flash！！
(setq ring-bell-function 'ignore)
(setq ring-bell-function
      (lambda ()
        (let ((orig-fg (face-background 'mode-line)))
          (set-face-background 'mode-line "purple4")
          (run-with-idle-timer 0.1 nil
                               (lambda (fg) (set-face-background 'mode-line fg))
                               orig-fg))))
;; save時にmode line を光らせる
(add-hook 'after-save-hook
          (lambda ()
            (let ((orig-fg (face-background 'mode-line)))
              (set-face-background 'mode-line "HotPink")
              (run-with-idle-timer 0.2 nil
                                   (lambda (fg) (set-face-background 'mode-line fg))
                                   orig-fg))))

;;; elscreenの見た目
(set-face-attribute 'elscreen-tab-current-screen-face nil
                    :height 130)
(set-face-attribute 'elscreen-tab-other-screen-face nil
                    :height 130
                    :foreground "black")
(setq elscreen-display-tab 10)
;;; [X]を表示しない
(setq elscreen-tab-display-kill-screen nil)
;;; [<->]を表示しない
(setq elscreen-tab-display-control nil)
;;; タブに表示させる内容を決定
(setq elscreen-buffer-to-nickname-alist
      '(("^dired-mode$" .
         (lambda ()
           (format "D(%s)" dired-directory)))
        ("^Info-mode$" .
         (lambda ()
           (format "Info(%s)" (file-name-nondirectory Info-current-file))))
        ("^mew-draft-mode$" .
         (lambda ()
           (format "Mew(%s)" (buffer-name (current-buffer)))))
        ("^mew-" . "Mew")
        ("^irchat-" . "IRChat")
        ("^liece-" . "Liece")
        ("^lookup-" . "Lookup")))
(setq elscreen-mode-to-nickname-alist
      '(("[Ss]hell" . "shell")
        ("compilation" . "compile")
        ("-telnet" . "telnet")
        ("dict" . "OnlineDict")
        ("*WL:Message*" . "Wanderlust")))

;;; カーソルをてからせるか否か
(blink-cursor-mode nil)

;;; すぐに次のキーバインドを表示する
(setq echo-keystrokes 0.1)

;; カーソル行をハイライト
;; (setq hl-line-face 'hlline-face)
;; (global-hl-line-mode)

(setq org-src-fontify-natively t)

(defun haml-compile()
  (interactive)
  (compile "haml" " " " --update " (concat "'" (match-string 0 buffer-file-name) "'")))

(bind-key* (kbd "C-M-n") 'forward-list)
(bind-key* (kbd "C-M-p") 'backward-list)

(bind-key* (kbd "C-x C-c") 'counsel-bookmark)
(bind-key* (kbd "C-x C-z") 'counsel-rg)

;; org-mode
;;; 折返し
(toggle-truncate-lines t)
(setq org-startup-truncated nil)
(bind-key* "C-c t" 'toggle-truncate-lines)

;;; exit-emacsで終了できるようにaliasを貼った
(defalias 'exit-emacs 'save-buffers-kill-terminal)

;;; quickrun
(setq quickrun-focus-p nil)
(global-set-key (kbd "<f6>") 'quickrun)
(quickrun-add-command "scss"
  '((:command . "sass")
    (:exec . "%c --no-cache --style expanded %s")
    (:compile-only . "%c %s"))
  :mode 'scss-mode)

;;; 相対パスコピー
(defun get-git-filepath ()
  "Get relative current path from git dir."
  (let* ((path buffer-file-name)
         (root (file-truename (vc-git-root path)))
         (filepath-from-git-root (file-relative-name path root)))
    (message filepath-from-git-root)))
(defun copy-current-git-path ()
  "Copy relative current path from git dir to clipboard."
  (interactive)
  (let ((path (get-git-filepath)))
    (when path
      (message "copied path: %s" path)
      (kill-new  path))))

;;; apple script関連
;; pathを通す

;; ChromeをReloadする
(defun my-reload-chrome()
  (interactive)
  (shell-command "~/applescript/reload_chrome"))
(bind-key* (kbd "<f5>") 'my-reload-chrome)

;; ChromeのTabを切り替える
(defun my-switch-chrome-tab()
  (interactive)
  (shell-command "~/applescript/switch_chrome_tab"))
(bind-key* (kbd "H-l") 'my-switch-chrome-tab)

;; ChromeのTabを切り替える
(defun my-switch-reverse-chrome-tab()
  (interactive)
  (shell-command "~/applescript/switch_reverse_chrome_tab"))
(bind-key* (kbd "H-h") 'my-switch-reverse-chrome-tab)

;; Chromeを下にスクロールする
(defun my-scroll-chrome-down()
  (interactive)
  (shell-command "~/applescript/scroll_chrome down"))
(bind-key* (kbd "H-j") 'my-scroll-chrome-down)

;; Chromeを上にスクロールする
(defun my-scroll-chrome-up()
  (interactive)
  (shell-command "~/applescript/scroll_chrome up"))
(bind-key* (kbd "H-k") 'my-scroll-chrome-up)

;;; undohist
(require 'undohist)
(undohist-initialize)
(setq undohist-ignored-files
      '("/tmp/" "COMMIT_EDITMSG"))

;;; kmacro
(bind-key* [f7] 'kmacro-start-macro)
(bind-key* [C-f7] 'kmacro-end-macro)

;;; winner-mode
(setq winner-mode-map (make-sparse-keymap))
(winner-mode 1)

;;; scrollを1行ずつ
(setq scroll-step 1)

;;; help key
(global-set-key (kbd "C-c C-h") 'help-command)

;;; point-undo
(require 'point-undo)

;;; 全てインデント
(defun all-indent ()
  (interactive)
  (mark-whole-buffer)
  (indent-region (region-beginning)(region-end))
  (point-undo))
(bind-key* (kbd "C-M-¥") 'all-indent)

;;; Finder by applescript
(defun phyten-mac-Finder-reveal (file)
  "Reveal (select/highlight) FILE in Finder."
  (interactive (list (or (buffer-file-name) ".")))
  ;; FIXME: It is better and easier to use 'open -R'
  (do-applescript
   (format (concat
            "tell application \"Finder\"\n"
            "	activate\n"
            "	reveal POSIX file \"%s\"\n"
            "end tell")
           (expand-file-name file))))

;;; url
(defun phyten-chrome-url ()
  "Return the URL of the current tab of Chrome."
  (replace-regexp-in-string
   (rx (or (and string-start ?\")
           (and ?\" string-end)))
   ""
   (do-applescript
    "tell application \"Google Chrome\" to return URL of active tab of first window")))

;;; ランダム6文字の文字列
(defun random-alnum ()
  (let* ((alnum "abcdefghijklmnopqrstuvwxyz0123456789")
         (i (% (abs (random)) (length alnum))))
    (substring alnum i (1+ i))))
(defun random-6-letter-string ()
  (interactive)
  (insert
   (concat
    (random-alnum)
    (random-alnum)
    (random-alnum)
    (random-alnum)
    (random-alnum)
    (random-alnum)
    )))

;;; ランダムな文字色
(defun phyten/copy-random-color ()
  "copy random-color"
  (interactive)
  (let ((color (format "#%02x%02x%02x"
                       (cl-random 255)
                       (cl-random 255)
                       (cl-random 255))))
    (message "copied color: %s" color)
    (kill-new  color)))
(defun phyten/yank-random-color ()
  "copy random-color"
  (interactive)
  (phyten/copy-random-color)
  (yank))

;;; bm.el
(setq-default bm-buffer-persistence nil)
(setq bm-restore-repository-on-load t)
(require 'bm)
(add-hook 'find-file-hook 'bm-buffer-restore)
(add-hook 'kill-buffer-hook 'bm-buffer-save)
(add-hook 'after-save-hook 'bm-buffer-save)
(add-hook 'after-revert-hook 'bm-buffer-restore)
(add-hook 'vc-before-checkin-hook 'bm-buffer-save)
(add-hook 'kill-emacs-hook '(lambda nil
                              (bm-buffer-save-all)
                              (bm-repository-save)))
(bind-key* (kbd "M-SPC") 'bm-toggle)
(bind-key* (kbd "M-[") 'bm-previous)
(bind-key* (kbd "M-]") 'bm-next)

(require 'cl-lib)
(require 'color)

(rainbow-delimiters-mode 1)
(setq rainbow-delimiters-outermost-only-face-count 1)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(rainbow-delimiters-depth-1-face ((t (:foreground "dark orange"))))
 '(rainbow-delimiters-depth-2-face ((t (:foreground "deep pink"))))
 '(rainbow-delimiters-depth-3-face ((t (:foreground "chartreuse"))))
 '(rainbow-delimiters-depth-4-face ((t (:foreground "deep sky blue"))))
 '(rainbow-delimiters-depth-5-face ((t (:foreground "yellow"))))
 '(rainbow-delimiters-depth-6-face ((t (:foreground "orchid"))))
 '(rainbow-delimiters-depth-7-face ((t (:foreground "spring green"))))
 '(rainbow-delimiters-depth-8-face ((t (:foreground "sienna1")))))

(with-eval-after-load 'rainbow-delimiters
  (require 'color)
  (dotimes (i (- rainbow-delimiters-max-face-count 1))
    (let ((face (rainbow-delimiters-default-pick-face (+ i 1) t nil)))
      (set-face-foreground face (color-saturate-name (face-foreground face) 60)))))
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)

(use-package smart-cursor-color
  :ensure t
  :config
  (smart-cursor-color-mode 1))

;;; diff-hlが悪さをしていたら止めること
;;; git-gutter
(use-package git-gutter
  :ensure t
  :custom
  (git-gutter:ask-p nil)                  ;いちいち聞かない
  (git-gutter:modified-sign "~")
  (git-gutter:added-sign    "+")
  (git-gutter:deleted-sign  "-")
  :custom-face
  (git-gutter:modified ((t (:background "#f1fa8c"))))
  (git-gutter:added    ((t (:background "#50fa7b"))))
  (git-gutter:deleted  ((t (:background "#ff79c6"))))
  )
(global-git-gutter-mode t)
(defun git-gutter:toggle-popup-hunk ()
  "Toggle git-gutter hunk window."
  (interactive)
  (if (window-live-p (git-gutter:popup-buffer-window))
      (delete-window (git-gutter:popup-buffer-window))
    (git-gutter:popup-hunk)))
;; Hydra
(bind-key
 "<f3>"
 (defhydra hydra-git-gutter (:color red :hint nil)
   "
_m_agit  _b_lame  _d_ispatch  _t_imemachine  |  hunk: _p_revious  _n_ext  _s_tage  _r_evert  pop_u_p  _SPC_:toggle  _H_:unk"
   ("m" magit-status :exit t)
   ("c" magit-status :exit t)
   ("b" magit-blame :exit t)
   ("t" git-timemachine :exit t)
   ("d" magit-dispatch :exit t)
   ("p" git-gutter:previous-hunk)
   ("n" git-gutter:next-hunk)
   ("s" git-gutter:stage-hunk)
   ("r" git-gutter:revert-hunk)
   ("u" git-gutter:popup-hunk)
   ("SPC" git-gutter:toggle-popup-hunk)
   ("H" git-gutter:popup-hunk)))

;;; display-line-numbers-mode
;;  it is lighter than linum-mode and can be used with git-gutter-mode.
;; (add-hook 'prog-mode-hook 'display-line-numbers-mode)
;; (add-hook 'text-mode-hook 'display-line-numbers-mode)
;; (bind-key [(ctrl f9)] 'display-line-numbers-mode)
(global-display-line-numbers-mode)
(bind-key [(ctrl f9)] 'global-display-line-numbers-mode)

(setq-default
 display-line-numbers-width 4
 display-line-numbers-widen t)

;;; company補完に日本語を含めないようにする
(defun edit-category-table-for-company-dabbrev (&optional table)
  (define-category ?s "word constituents for company-dabbrev" table)
  (let ((i 0))
    (while (< i 128)
      (if (equal ?w (char-syntax i))
          (modify-category-entry i ?s table)
        (modify-category-entry i ?s table t))
      (setq i (1+ i)))))
(edit-category-table-for-company-dabbrev)
;; (add-hook 'TeX-mode-hook 'edit-category-table-for-company-dabbrev) ; 下の追記参照
(setq company-dabbrev-char-regexp "\\cs")

(require 'prettier-js)
(add-hook 'js2-mode-hook 'prettier-js-mode)
(defface me/js2-async-face
  '((t :foreground "white"))
  "js2 keywords on async")
(set-face-underline 'me/js2-async-face t)
(add-hook 'js2-mode-hook
          (lambda ()
            (highlight-phrase "async" 'me/js2-async-face)
            (highlight-phrase "await" 'me/js2-async-face)
            ))

;;; disable-mouse.el
;;; マウスを無効化
;; (use-package disable-mouse
;;   :ensure t
;;   :config
;;   (global-disable-mouse-mode))

;;; typescript
(package-install 'tide)
(require 'tide)
(add-hook 'typescript-mode-hook
          (lambda ()
            (interactive)
            (setq typescript-indent-level 2)
            (setq typescript-expr-indent-offset 2)
            (tide-setup)
            (flycheck-mode +1)
            (setq flycheck-check-syntax-automatically '(save mode-enabled))
            (eldoc-mode +1)
            (tide-hl-identifier-mode +1)
            (company-mode +1)
            (global-set-key (kbd "M-*") 'tide-jump-back)))
(add-hook 'before-save-hook 'tide-format-before-save)

;;; 自動保存ファイルの制御
(setq auto-save-file-name-transforms   '((".*" "~/tmp/" t)))
(setq auto-save-timeout 10)             ;10 seconds
(setq auto-save-interval 100)           ;100 inputs

;;; quoted-insert
(bind-key* "C-q C-q" 'quoted-insert)

;;; macのpinchを無効
(global-unset-key (kbd "<magnify-down>"))
(global-unset-key (kbd "<magnify-up>"))

(require 'docker-tramp)
(set-variable 'docker-tramp-use-names t)

(setq browse-url-browser-function 'browse-url-default-macosx-browser
      browse-url-new-window-flag  t
      browse-url-firefox-new-window-is-tab t)

(defun toggle-org-html-export-on-save ()
  (interactive)
  (if (memq 'org-html-export-to-html after-save-hook)
      (progn
        (remove-hook 'after-save-hook 'org-html-export-to-html t)
        (message "Disabled org html export on save for current buffer..."))
    (add-hook 'after-save-hook 'org-html-export-to-html nil t)
    (add-hook 'org-html-export-to-html 'my-reload-chrome nil t)
    (message "Enabled org html export on save for current buffer..."))
  )

;; magit
(global-set-key (kbd "C-c C-g") 'magit-diff-working-tree)

;; ファイル編集時に，bufferを再読込
(global-auto-revert-mode 1)

;;; subshell-proc
(require 'subshell-proc)
(defproc "scss-base" "rsync" (list "--exclude" ".git" "-av" "/Users/phyten/development/scss_base/" (s-replace "~" "/Users/phyten" (file-name-directory default-directory))))
(defproc "echo-test" "echo" (list (s-replace "~" "/Users/phyten" (file-name-directory default-directory))))

;;; doom-themes
(package-install 'doom-themes)
(require 'doom-themes)
;; Global settings (defaults)
(setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
      doom-themes-enable-italic t) ; if nil, italics is universally disabled
;; Load the theme (doom-one, doom-molokai, etc); keep in mind that each theme
;; may have their own settings.
(load-theme 'doom-dracula t)
;; Enable flashing mode-line on errors
(doom-themes-visual-bell-config)
;; Enable custom neotree theme (all-the-icons must be installed!)
(doom-themes-neotree-config)
;; or for treemacs users
(setq doom-themes-treemacs-theme "doom-colors") ; use the colorful treemacs theme
(doom-themes-treemacs-config)
;; Corrects (and improves) org-mode's native fontification.
(doom-themes-org-config)
