(when *is-a-mac*
  (maybe-require-package 'grab-mac-link))

;;; Org modeの設定
;;; ファイルの場所
(setq org-directory "/Users/phyten/Dropbox/org")
(setq org-default-notes-file "todo.org")

;; C-ca で org-agenda
(bind-key* "C-c a" 'org-agenda)
;; 標準の祝日を利用しない
(setq calendar-holidays nil)
;; TODO状態
(setq org-todo-keywords
      '((sequence "NEXT(n)" "TODO(t)" "DOING(d)" "WAIT(w)" "SOMEDAY(s)" "PENDING(p)" "|" "DONE(o)" "CANCELED(c)")))
;; DONEの時刻を記録
(setq org-log-done 'time)
;; タグリスト
(setq org-tag-alist '(("meeting" . ?m) ("office" . ?o) ("document" . ?d) ("kitting" . ?k) ("study" . ?s) ("travel" . ?t) ))
;; アジェンダ表示の対象ファイル
(setq org-agenda-files '(
                         "/Users/phyten/Dropbox/org/aaa.org"
                         "/Users/phyten/Dropbox/org/anken"
                         "/Users/phyten/Dropbox/org/todo.org"
                         "/Users/phyten/Dropbox/org/summary.org"
                         "/Users/phyten/Dropbox/org/knowledge"
                         ))

;;; Org-captureの設定
;;; Org-captureを呼び出すキーシーケンス
(bind-key* "C-c c" 'org-capture)
;;; Org-captureのテンプレート（メニュー）の設定
(setq org-capture-templates
      '(
        ("s" "Summary" entry (file+headline "/Users/phyten/Dropbox/org/Summary.org" "Drafted Summaries")
         "* TODO %?\n# Wrote on %U")
        ("t" "Todo" entry (file+headline "/Users/phyten/Dropbox/org/todo.org" "Misc")
         "* TODO %? %U\n")
        ("j" "JohoKojo" entry (file+headline "/Users/phyten/Dropbox/org/joho-kojo.org" "JohoKojo")
         "* TODO %? %U\n")
        ("i" "idea" entry (file+headline "/Users/phyten/Dropbox/org/idea.org" "idea")
         "* TODO %? \n%[/Users/phyten/Dropbox/org/templates/idea_template.org]")
        ("c" "techacademy_chat" entry (file+headline "/Users/phyten/Dropbox/org/techacademy.org" "chat")
         "* %?\n# Wrote on %U")
        ("n" "note" entry (file "")
         "* %? :NOTE:\n%U\n%a\n" :clock-resume t)
        ("k" "Knowledge" entry (file+headline "/Users/phyten/Dropbox/org/knowledge.org" "TOP")
         "* %?\n# Wrote on %U")
        ("r" "Ruby" entry (file+headline "/Users/phyten/Dropbox/org/knowledge/ruby.org" "RubyKnowledge")
         "* %?\n# Wrote on %U")
        ("d" "Docker" entry (file+headline "/Users/phyten/Dropbox/org/knowledge/docker.org" "分類待ち")
         "* %?\n# Wrote on %U")
        ("o" "Org-mode" entry (file+headline "/Users/phyten/Dropbox/org/knowledge/org-mode.org" "TOP")
         "* %?\n# Wrote on %U")
        ("e" "Emacslisp" entry (file+headline "/Users/phyten/Dropbox/org/knowledge/emacslisp.org" "TOP")
         "* %?\n# Wrote on %U")
        ))

;; (setq org-capture-templates
;;       '(("n" "Note" entry (file+headline "/Users/phyten/Dropbox/org/notes.org" "Notes")
;;          "* %?\nEntered on %U\n %i\n %a")
;;         ))
;; (setq org-capture-templates
;;       `(("t" "Todo" entry (file+headline "/Users/phyten/Dropbox/org/todo.org" "■TODOs")
;;          "* TODO %? (wrote on %U)")
;;         ("n" "note" entry (file "")
;;          "* %? :NOTE:\n%U\n%a\n" :clock-resume t)
;;         ))


;;; メモをC-M-^一発で見るための設定
(defun show-org-buffer (file)
  "Show an org-file FILE on the current buffer."
  (interactive)
  (if (get-buffer file)
      (let ((buffer (get-buffer file)))
        (switch-to-buffer buffer)
        (message "%s" file))
    (find-file (concat "/Users/phyten/Dropbox/org/" file))))
(bind-key* "C-M-^" '(lambda () (interactive)
                      (show-org-buffer "todo.org")))

;; org-clock
;; clockログを隠す
(setq org-clock-into-drawer t)
;; clocktable の体裁を整える
(defun my-org-clocktable-indent-string (level)
  (if (= level 1) ""
    (let ((str " "))
      (while (> level 2)
        (setq level (1- level)
              str (concat str "--")))
      (concat str "-> "))))

(advice-add 'org-clocktable-indent-string :override #'my-org-clocktable-indent-string)

(defvar org-export-output-directory-prefix "export_" "prefix of directory used for org-mode export")

(defadvice org-export-output-file-name (before org-add-export-dir activate)
  "Modifies org-export to place exported files in a different directory"
  (when (not pub-dir)
    (setq pub-dir (concat org-export-output-directory-prefix (substring extension 1)))
    (when (not (file-directory-p pub-dir))
      (make-directory pub-dir))))

(defun export-org-md-command-events ()
  (interactive)
  (let* ((md-original-filename (concat (file-name-directory buffer-file-name) "export_md/" (file-name-sans-extension (file-name-nondirectory buffer-file-name)) ".md"))
         (google-drive-filename (concat  "../Google Drive/notes/" (file-name-sans-extension (file-name-nondirectory buffer-file-name)) ".md")))
    (execute-kbd-macro (kbd "C-c C-e m m"))
    (copy-file  md-original-filename google-drive-filename t)
    (message  "Saved to Google Drive: %s" (concat (file-name-sans-extension (file-name-nondirectory buffer-file-name)) ".md")) )
  )

;; Auto-export org files to Markdown when saved
(defun org-mode-export-hook ()
  (when (equal major-mode 'org-mode)
    (add-hook 'after-save-hook 'export-org-md-command-events t t)
    )
  )

(defun markdown-preview-file ()
  "run Marked on the current file and revert the buffer"
  (interactive)
  (execute-kbd-macro (kbd "C-c C-e m m"))
  (shell-command
   (format "open -a /Applications/Marked2.app %s"
           (shell-quote-argument (concat (file-name-directory buffer-file-name) "export_md/" (file-name-sans-extension (file-name-nondirectory buffer-file-name)) ".md"))))
  )

(global-set-key (kbd "C-c m") 'markdown-preview-file)
(maybe-require-package 'org-cliplink)

(define-key global-map (kbd "C-c l") 'org-store-link)
;; (define-key global-map (kbd "C-c a") 'org-agenda)

;; Various preferences
(setq org-log-done t
      org-edit-timestamp-down-means-later t
      org-archive-mark-done nil
      org-hide-emphasis-markers t
      org-catch-invisible-edits 'show
      org-export-coding-system 'utf-8
      org-fast-tag-selection-single-key 'expert
      org-html-validation-link nil
      org-export-kill-product-buffer-when-displayed t
      org-tags-column 80)


;; ;; Lots of stuff from http://doc.norang.ca/org-mode.html

;; ;; TODO: fail gracefully
;; (defun sanityinc/grab-ditaa (url jar-name)
;;   "Download URL and extract JAR-NAME as `org-ditaa-jar-path'."
;;   ;; TODO: handle errors
;;   (message "Grabbing " jar-name " for org.")
;;   (let ((zip-temp (make-temp-name "emacs-ditaa")))
;;     (unwind-protect
;;         (progn
;;           (when (executable-find "unzip")
;;             (url-copy-file url zip-temp)
;;             (shell-command (concat "unzip -p " (shell-quote-argument zip-temp)
;;                                    " " (shell-quote-argument jar-name) " > "
;;                                    (shell-quote-argument org-ditaa-jar-path)))))
;;       (when (file-exists-p zip-temp)
;;         (delete-file zip-temp)))))



;; (after-load 'ob-plantuml
;;   (let ((jar-name "plantuml.jar")
;;         (url "http://jaist.dl.sourceforge.net/project/plantuml/plantuml.jar"))
;;     (setq org-plantuml-jar-path (expand-file-name jar-name (file-name-directory user-init-file)))
;;     (unless (file-exists-p org-plantuml-jar-path)
;;       (url-copy-file url org-plantuml-jar-path))))



;; 

;; (maybe-require-package 'writeroom-mode)

;; (define-minor-mode prose-mode
;;   "Set up a buffer for prose editing.
;; This enables or modifies a number of settings so that the
;; experience of editing prose is a little more like that of a
;; typical word processor."
;;   nil " Prose" nil
;;   (if prose-mode
;;       (progn
;;         (when (fboundp 'writeroom-mode)
;;           (writeroom-mode 1))
;;         (setq truncate-lines nil)
;;         (setq word-wrap t)
;;         (setq cursor-type 'bar)
;;         (when (eq major-mode 'org)
;;           (kill-local-variable 'buffer-face-mode-face))
;;         (buffer-face-mode 1)
;;         ;;(delete-selection-mode 1)
;;         (set (make-local-variable 'blink-cursor-interval) 0.6)
;;         (set (make-local-variable 'show-trailing-whitespace) nil)
;;         (set (make-local-variable 'line-spacing) 0.2)
;;         (set (make-local-variable 'electric-pair-mode) nil)
;;         (ignore-errors (flyspell-mode 1))
;;         (visual-line-mode 1))
;;     (kill-local-variable 'truncate-lines)
;;     (kill-local-variable 'word-wrap)
;;     (kill-local-variable 'cursor-type)
;;     (kill-local-variable 'show-trailing-whitespace)
;;     (kill-local-variable 'line-spacing)
;;     (kill-local-variable 'electric-pair-mode)
;;     (buffer-face-mode -1)
;;     ;; (delete-selection-mode -1)
;;     (flyspell-mode -1)
;;     (visual-line-mode -1)
;;     (when (fboundp 'writeroom-mode)
;;       (writeroom-mode 0))))

;; ;;(add-hook 'org-mode-hook 'buffer-face-mode)


;; (setq org-support-shift-select t)
;; 
;; ;;; Capturing

;; (global-set-key (kbd "C-c c") 'org-capture)

;; (setq org-capture-templates
;;       `(("t" "todo" entry (file "")  ; "" => `org-default-notes-file'
;;          "* NEXT %?\n%U\n" :clock-resume t)
;;         ("n" "note" entry (file "")
;;          "* %? :NOTE:\n%U\n%a\n" :clock-resume t)
;;         ))


;; 
;;; Refiling

(setq org-refile-use-cache nil)

;; Targets include this file and any file contributing to the agenda - up to 5 levels deep
(setq org-refile-targets '((nil :maxlevel . 5) (org-agenda-files :maxlevel . 5)))

(after-load 'org-agenda
  (add-to-list 'org-agenda-after-show-hook 'org-show-entry))

(defadvice org-refile (after sanityinc/save-all-after-refile activate)
  "Save all org buffers after each refile operation."
  (org-save-all-org-buffers))

;; Exclude DONE state tasks from refile targets
(defun sanityinc/verify-refile-target ()
  "Exclude todo keywords with a done state from refile targets."
  (not (member (nth 2 (org-heading-components)) org-done-keywords)))
(setq org-refile-target-verify-function 'sanityinc/verify-refile-target)

(defun sanityinc/org-refile-anywhere (&optional goto default-buffer rfloc msg)
  "A version of `org-refile' which allows refiling to any subtree."
  (interactive "P")
  (let ((org-refile-target-verify-function))
    (org-refile goto default-buffer rfloc msg)))

(defun sanityinc/org-agenda-refile-anywhere (&optional goto rfloc no-update)
  "A version of `org-agenda-refile' which allows refiling to any subtree."
  (interactive "P")
  (let ((org-refile-target-verify-function))
    (org-agenda-refile goto rfloc no-update)))

;; Targets start with the file name - allows creating level 1 tasks
;;(setq org-refile-use-outline-path (quote file))
(setq org-refile-use-outline-path t)
(setq org-outline-path-complete-in-steps nil)

;; Allow refile to create parent tasks with confirmation
(setq org-refile-allow-creating-parent-nodes 'confirm)

;; 
;;; To-do settings
;;; 別の設定を使うから不要
;; (setq org-todo-keywords
;;       (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!/!)")
;;               (sequence "PROJECT(p)" "|" "DONE(d!/!)" "CANCELLED(c@/!)")
;;               (sequence "WAITING(w@/!)" "DELEGATED(e!)" "HOLD(h)" "|" "CANCELLED(c@/!)")))
;;       org-todo-repeat-to-state "NEXT")

;; (setq org-todo-keyword-faces
;;       (quote (("NEXT" :inherit warning)
;;               ("PROJECT" :inherit font-lock-string-face))))


;; 
;;; Agenda views

;; (setq-default org-agenda-clockreport-parameter-plist '(:link t :maxlevel 3))


;; (let ((active-project-match "-INBOX/PROJECT"))

;;   (setq org-stuck-projects
;;         `(,active-project-match ("NEXT")))

;;   (setq org-agenda-compact-blocks t
;;         org-agenda-sticky t
;;         org-agenda-start-on-weekday nil
;;         org-agenda-span 'day
;;         org-agenda-include-diary nil
;;         org-agenda-sorting-strategy
;;         '((agenda habit-down time-up user-defined-up effort-up category-keep)
;;           (todo category-up effort-up)
;;           (tags category-up effort-up)
;;           (search category-up))
;;         org-agenda-window-setup 'current-window
;;         org-agenda-custom-commands
;;         `(("N" "Notes" tags "NOTE"
;;            ((org-agenda-overriding-header "Notes")
;;             (org-tags-match-list-sublevels t)))
;;           ("g" "GTD"
;;            ((agenda "" nil)
;;             (tags "INBOX"
;;                   ((org-agenda-overriding-header "Inbox")
;;                    (org-tags-match-list-sublevels nil)))
;;             (stuck ""
;;                    ((org-agenda-overriding-header "Stuck Projects")
;;                     (org-agenda-tags-todo-honor-ignore-options t)
;;                     (org-tags-match-list-sublevels t)
;;                     (org-agenda-todo-ignore-scheduled 'future)))
;;             (tags-todo "-INBOX"
;;                        ((org-agenda-overriding-header "Next Actions")
;;                         (org-agenda-tags-todo-honor-ignore-options t)
;;                         (org-agenda-todo-ignore-scheduled 'future)
;;                         (org-agenda-skip-function
;;                          '(lambda ()
;;                             (or (org-agenda-skip-subtree-if 'todo '("HOLD" "WAITING"))
;;                                 (org-agenda-skip-entry-if 'nottodo '("NEXT")))))
;;                         (org-tags-match-list-sublevels t)
;;                         (org-agenda-sorting-strategy
;;                          '(todo-state-down effort-up category-keep))))
;;             (tags-todo ,active-project-match
;;                        ((org-agenda-overriding-header "Projects")
;;                         (org-tags-match-list-sublevels t)
;;                         (org-agenda-sorting-strategy
;;                          '(category-keep))))
;;             (tags-todo "-INBOX/-NEXT"
;;                        ((org-agenda-overriding-header "Orphaned Tasks")
;;                         (org-agenda-tags-todo-honor-ignore-options t)
;;                         (org-agenda-todo-ignore-scheduled 'future)
;;                         (org-agenda-skip-function
;;                          '(lambda ()
;;                             (or (org-agenda-skip-subtree-if 'todo '("PROJECT" "HOLD" "WAITING" "DELEGATED"))
;;                                 (org-agenda-skip-subtree-if 'nottododo '("TODO")))))
;;                         (org-tags-match-list-sublevels t)
;;                         (org-agenda-sorting-strategy
;;                          '(category-keep))))
;;             (tags-todo "/WAITING"
;;                        ((org-agenda-overriding-header "Waiting")
;;                         (org-agenda-tags-todo-honor-ignore-options t)
;;                         (org-agenda-todo-ignore-scheduled 'future)
;;                         (org-agenda-sorting-strategy
;;                          '(category-keep))))
;;             (tags-todo "/DELEGATED"
;;                        ((org-agenda-overriding-header "Delegated")
;;                         (org-agenda-tags-todo-honor-ignore-options t)
;;                         (org-agenda-todo-ignore-scheduled 'future)
;;                         (org-agenda-sorting-strategy
;;                          '(category-keep))))
;;             (tags-todo "-INBOX"
;;                        ((org-agenda-overriding-header "On Hold")
;;                         (org-agenda-skip-function
;;                          '(lambda ()
;;                             (or (org-agenda-skip-subtree-if 'todo '("WAITING"))
;;                                 (org-agenda-skip-entry-if 'nottodo '("HOLD")))))
;;                         (org-tags-match-list-sublevels nil)
;;                         (org-agenda-sorting-strategy
;;                          '(category-keep))))
;;             ;; (tags-todo "-NEXT"
;;             ;;            ((org-agenda-overriding-header "All other TODOs")
;;             ;;             (org-match-list-sublevels t)))
;;             )))))


;; (add-hook 'org-agenda-mode-hook 'hl-line-mode)

;; 
;;; Org clock

;; Save the running clock and all clock history when exiting Emacs, load it on startup
(after-load 'org
  (org-clock-persistence-insinuate))
(setq org-clock-persist t)
(setq org-clock-in-resume t)

;; Save clock data and notes in the LOGBOOK drawer
(setq org-clock-into-drawer t)
;; Save state changes in the LOGBOOK drawer
(setq org-log-into-drawer t)
;; Removes clocked tasks with 0:00 duration
(setq org-clock-out-remove-zero-time-clocks t)

;; Show clock sums as hours and minutes, not "n days" etc.
(setq org-time-clocksum-format
      '(:hours "%d" :require-hours t :minutes ":%02d" :require-minutes t))


;; 
;;; Show the clocked-in task - if any - in the header line
(defun sanityinc/show-org-clock-in-header-line ()
  (setq-default header-line-format '((" " org-mode-line-string " "))))

(defun sanityinc/hide-org-clock-from-header-line ()
  (setq-default header-line-format nil))

(add-hook 'org-clock-in-hook 'sanityinc/show-org-clock-in-header-line)
(add-hook 'org-clock-out-hook 'sanityinc/hide-org-clock-from-header-line)
(add-hook 'org-clock-cancel-hook 'sanityinc/hide-org-clock-from-header-line)

(after-load 'org-clock
  (define-key org-clock-mode-line-map [header-line mouse-2] 'org-clock-goto)
  (define-key org-clock-mode-line-map [header-line mouse-1] 'org-clock-menu))


;; 
;; (when (and *is-a-mac* (file-directory-p "/Applications/org-clock-statusbar.app"))
;;   (add-hook 'org-clock-in-hook
;;             (lambda () (call-process "/usr/bin/osascript" nil 0 nil "-e"
;;                                      (concat "tell application \"org-clock-statusbar\" to clock in \"" org-clock-current-task "\""))))
;;   (add-hook 'org-clock-out-hook
;;             (lambda () (call-process "/usr/bin/osascript" nil 0 nil "-e"
;;                                      "tell application \"org-clock-statusbar\" to clock out"))))

(add-hook 'org-clock-in-hook (lambda () (call-process "/usr/bin/osascript" nil 0 nil "-e" (concat "tell application \"org-clock-statusbar\" to clock in \"" (replace-regexp-in-string "\"" "\\\\\"" org-clock-current-task) "\""))))
(add-hook 'org-clock-out-hook (lambda () (call-process "/usr/bin/osascript" nil 0 nil "-e" "tell application \"org-clock-statusbar\" to clock out")))

;; 
;; ;; TODO: warn about inconsistent items, e.g. TODO inside non-PROJECT
;; ;; TODO: nested projects!


;; 
;;; Archiving

(setq org-archive-mark-done nil)
(setq org-archive-location "%s_archive::* Archive")



;; 

(require-package 'org-pomodoro)
(setq org-pomodoro-keep-killed-pomodoro-time t)
(after-load 'org-agenda
  (define-key org-agenda-mode-map (kbd "P") 'org-pomodoro))


;; ;; Show iCal calendars in the org agenda
;; (when (and *is-a-mac* (require 'org-mac-iCal nil t))
;;   (setq org-agenda-include-diary t
;;         org-agenda-custom-commands
;;         '(("I" "Import diary from iCal" agenda ""
;;            ((org-agenda-mode-hook #'org-mac-iCal)))))

;;   (add-hook 'org-agenda-cleanup-fancy-diary-hook
;;             (lambda ()
;;               (goto-char (point-min))
;;               (save-excursion
;;                 (while (re-search-forward "^[a-z]" nil t)
;;                   (goto-char (match-beginning 0))
;;                   (insert "0:00-24:00 ")))
;;               (while (re-search-forward "^ [a-z]" nil t)
;;                 (goto-char (match-beginning 0))
;;                 (save-excursion
;;                   (re-search-backward "^[0-9]+:[0-9]+-[0-9]+:[0-9]+ " nil t))
;;                 (insert (match-string 0))))))


(after-load 'org
  (define-key org-mode-map (kbd "C-M-<up>") 'org-up-element)
  (when *is-a-mac*
    (define-key org-mode-map (kbd "M-h") nil)
    (define-key org-mode-map (kbd "C-c g") 'grab-mac-link-dwim)))

(after-load 'org
  (org-babel-do-load-languages
   'org-babel-load-languages
   `((R . t)
     (ditaa . t)
     (dot . t)
     (emacs-lisp . t)
     (gnuplot . t)
     (haskell . nil)
     (latex . t)
     (ledger . t)
     (ocaml . nil)
     (octave . t)
     (plantuml . t)
     (python . t)
     (ruby . t)
     (screen . nil)
     (,(if (locate-library "ob-sh") 'sh 'shell) . t)
     (sql . nil)
     (sqlite . t))))


(defun org-toggle-link-display ()
  "Toggle the literal or descriptive display of links."
  (interactive)
  (if org-descriptive-links
      (progn (org-remove-from-invisibility-spec '(org-link))
             (org-restart-font-lock)
             (setq org-descriptive-links nil))
    (progn (add-to-invisibility-spec '(org-link))
           (org-restart-font-lock)
           (setq org-descriptive-links t))))

(add-hook 'org-mode-hook #'org-make-toc-mode)

;;; 見かけ

;; 見出しをインデントする
(setq org-startup-indented t)
;; 見出しをインデントした時にアスタリスクが減るのを防ぐ
(setq org-indent-mode-turns-on-hiding-stars nil)
;; インデントの幅を設定
(setq org-indent-indentation-per-level 4)
;; 見出しの初期状態（見出しだけ表示）
(setq org-startup-folded 'content)
;; これは全部表示
;; (setq org-startup-folded 'showall)

(setq org-html-htmlize-output-type 'css)

;; these paths are for OSX homebrew installs
;; edit as needed...
;; (setq org-ditaa-jar-path "/usr/local/bin/ditaa")
(setq org-ditaa-jar-path "/Users/phyten/.emacs.d/ditaa0_9.jar")
(setq org-plantuml-jar-path "/usr/local/bin/plantuml")

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (clojure . t)
   ;; (sh . t)
   (ditaa . t)
   (dot . t)
   (plantuml . t)))

(message "loaded config")

(setq org-emphasis-regexp-components
      ;; markup 记号前后允许中文
      (list (concat " \t('\"{"            "[:nonascii:]")
            (concat "- \t.,:!?;'\")}\\["  "[:nonascii:]")
            " \t\r\n,\"'"
            "."
            1))

;;; child todo depended on parent todo
(setq org-enforce-todo-dependencies t)

(setq org-confirm-babel-evaluate nil)

(provide 'init-org)
