;; ;; Support for the http://kapeli.com/dash documentation browser

;; (defun sanityinc/dash-installed-p ()
;;   "Return t if Dash is installed on this machine, or nil otherwise."
;;   (let ((lsregister "/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/LaunchServices.framework/Versions/A/Support/lsregister"))
;;     (and (file-executable-p lsregister)
;;          (not (string-equal
;;                ""
;;                (shell-command-to-string
;;                 (concat lsregister " -dump|grep com.kapeli.dash")))))))

;; (when (and *is-a-mac* (not (package-installed-p 'dash-at-point)))
;;   (message "Checking whether Dash is installed")
;;   (when (sanityinc/dash-installed-p)
;;     (require-package 'dash-at-point)))

;; (when (package-installed-p 'dash-at-point)
;;   (global-set-key (kbd "C-c D") 'dash-at-point))


(require 'counsel-dash)

;;; きちんとdocsetを入れないと下記は発動しない。
;;; docsetは下記コマンドで導入する。
;;; M-x counsel-dash-install-docset
(add-hook 'emacs-lisp-mode-hook (lambda () (setq-local counsel-dash-docsets '("Emacs Lisp"))))
(add-hook 'ruby-mode-hook (lambda () (setq-local counsel-dash-docsets '("Ruby" "Ruby on Rails"))))
(add-hook 'js2-mode-hook (lambda () (setq-local counsel-dash-docsets '("JavaScript" "jQuery" "VueJS"))))
(add-hook 'css-mode-hook (lambda () (setq-local counsel-dash-docsets '("CSS"))))
(add-hook 'scss-mode-hook (lambda () (setq-local counsel-dash-docsets '("CSS"))))

;;; options
;; (setq counsel-dash-docsets-path "~/.docset")
;; (setq counsel-dash-docsets-url "https://raw.github.com/Kapeli/feeds/master")
;; (setq counsel-dash-min-length 3)
;; (setq counsel-dash-candidate-format "%d %n (%t)")
;; (setq counsel-dash-enable-debugging nil)
;; (setq counsel-dash-browser-func 'browse-url)
;; (setq counsel-dash-ignored-docsets nil)

(bind-key* [f4] 'counsel-dash)

(provide 'init-dash)
