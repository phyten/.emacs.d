(add-hook 'after-init-hook 'recentf-mode)
(setq-default
 recentf-max-saved-items 1000
 recentf-exclude '("/recentf" "COMMIT_EDITMSG" "/.?TAGS" "^/sudo:" "/\\.emacs\\.d/games/*-scores" "/\\.emacs\\.d/\\.cask/" "/tmp/")
 )

(setq recentf-auto-save-timer (run-with-idle-timer 30 t 'recentf-save-list))

(provide 'init-recentf)
